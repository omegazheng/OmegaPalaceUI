/**
 * Helper Function to include template files in html
 */

 function loadParts(){
 	$('[data-part]').each(function(index, el) {
		var tpl = 'parts/' + $(el).data('part') + '.html';
		$.get(tpl, function(data) {
		  if(data){
		  	$(el).replaceWith(data);
		  }
		});
	});
 }

jQuery(document).ready(function($) {
	loadParts();
});

