(function ($) {
    $.fn.toggleDisabled = function () {
        return this.each(function () {
            var $this = $(this);
            $this.prop('disabled', !$this.prop('disabled')) 
        });
    };
})(jQuery);


var gamesPage = (function($, _) {

	var gamesGridLayouts = {
			l1: [
				{ className: "col-xs-10 offset-xs-1" }
			],
			l2: [
				{ className: "col-xs-6" },
				{ className: "col-xs-6" }
			],
			l3: [
				{ className: "col-xs-8" },
				{ className: "col-xs-4" },
				{ className: "col-xs-4" }
			],
			l4: [
				{ className: "col-xs-5 offset-xs-1" },
				{ className: "col-xs-5" },
				{ className: "col-xs-5 offset-xs-1" },
				{ className: "col-xs-5" }
			]
		},

		tplGameContainer = _.template($("script#tplGameContainer").html()),
		tplGameSelector = _.template($("script#tplGameSelector").html()),
		tplGameIframe = _.template($("script#tplGameIframe").html()),
		gamesGrid = $('#gamesGrid'),
		gamesGridSwitcher = $('#gamesGridSwitcher'),

		setLayout = function(layoutName) {
			var layout = gamesGridLayouts[layoutName],
				gamesGridRow = $('> .row', gamesGrid),
				prevQty = $('> div', gamesGridRow).length,
				newQty = layout.length,
				removeCols = Math.max(0, prevQty - newQty),
				addCols = Math.max(0, newQty - prevQty);

			//remove columns
			if (removeCols) {
				gamesGridRow.find("> div:nth-last-child(-n+" + removeCols + ")").remove();​
			}

			//add columns
			if (addCols) {
				for (i = 0; i < addCols; i++) {
					gamesGridRow.append('<div/>');
				}
			}

			//add layout class
			gamesGridRow.attr('class', 'row games-grid-' + layoutName);

			//add / remove games
			$('> div', gamesGridRow).each(function(index, el) {
				var col = $(this);
				col.attr('class', layout[index].className);
				if (!col.children().length) {
					col.append(tplGameContainer({ body: tplGameSelector() }));
				}
			});

			$('.game-select', gamesGridRow).select2();

		},

		loadGame = function(el) {

			var src = el.val(),
				container = el.closest('.game-container');

			$('.game-container-body', container).empty().append(tplGameIframe({ src: src }));

			$('.game-container-head button', container).toggleDisabled();

		},

		closeGame = function(el) {

			var container = el.closest('.game-container');

			$('.game-container-body', container)
				.empty()
				.append(tplGameSelector());

			$('.game-container-head button', container).toggleDisabled();
		};

	return {
		setLayout: setLayout,
		init: function() {

			gamesGrid
				.on('change', '.game-select', function(event) {
					event.preventDefault();
					loadGame($(this));
				})
				.on('click', '[data-action=closeGame]', function(event) {
					event.preventDefault();
					closeGame($(this));
				});

			gamesGridSwitcher.on('click', 'button', function(event) {
				event.preventDefault();
				var layoutName = $(this).data('layout');
				gamesPage.setLayout(layoutName);
			});

			gamesPage.setLayout('l1');

		}
	};

})(jQuery, _);

jQuery(document).ready(gamesPage.init);
