jQuery(document).ready(function($) {
	var closeMarkup = '<button title="%title%" type="button" class="modal-close mfp-close"></button>';
	$('[data-modal=ajax]').magnificPopup({
		type: 'ajax',
		overflowY: 'scroll',
		closeMarkup: closeMarkup,
		callbacks: {
			ajaxContentAdded: function() {
				loadParts();
			}
		}
	});
	$('[data-modal=iframe]').magnificPopup({
		type: 'iframe',
		iframe: {
			patterns: {
				custom: {
					index: '',
					src: '%id%'
				}
			}
		},
		alignTop: true,
		overflowY: 'scroll',
		closeMarkup: closeMarkup,
	});
});
