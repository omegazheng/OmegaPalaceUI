var swiper = new Swiper('.swiper-header > .swiper-container', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    mousewheelConrol: true,
    mousewheelForceToAxis: true,
});

var swiperWinners = new Swiper('.swiper-our-winners', {
    //pagination: '.swiper-pagination',
    paginationClickable: true,
    mousewheelConrol: true,
    mousewheelForceToAxis: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
});

var swiper = new Swiper('.swiper-container-', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    mousewheelConrol: true,
    mousewheelForceToAxis: true,
    //nextButton: '.swiper-button-next',
    //prevButton: '.swiper-button-prev',
    breakpoints: {
        // when window width is <= 480px
        480: {
            slidesPerView: 1,
            spaceBetweenSlides: 20
        },
        // when window width is <= 900px
        900: {
            slidesPerView: 2,
            spaceBetweenSlides: 20
        },
        // when window width is <= 1000px
        1000: {
            slidesPerView: 3,
            spaceBetweenSlides: 20
        }
    }
});


var swiperBonus = new Swiper('.swiper-bonus', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    breakpoints: {
        // when window width is <= 480px
        480: {
            slidesPerView: 1,
            spaceBetweenSlides: 50
        },
        // when window width is <= 900px
        900: {
            slidesPerView: 1,
            spaceBetweenSlides: 50
        },
        // when window width is <= 1000px
        1000: {
            slidesPerView: 1,
            spaceBetweenSlides: 50
        }
    }
});

if ($(window).width() < 768) {

    //remove columns
    $('.col-md-6', '.swiper-about').removeClass('col-md-6');

    var swiperAbout = new Swiper('.swiper-about', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        mousewheelControl: true,
        mousewheelSensitivity: 8,
        mousewheelForceToAxis: true,
        breakpoints: {
            // when window width is <= 480px
            480: {
                slidesPerView: 1,
                spaceBetweenSlides: 20
            },
            // when window width is <= 900px
            900: {
                slidesPerView: 2,
                spaceBetweenSlides: 20
            },
            // when window width is <= 1000px
            1000: {
                slidesPerView: 3,
                spaceBetweenSlides: 20
            }
        }
    });
}else{
    //remove .swiper-slide
    $('.swiper-slide', '.swiper-about').removeClass('swiper-slide');
}




$(document).ready(function() {
    $('.menu-link').bigSlide();
});
