// This shows a full config file!
module.exports = function(grunt) {

    var mq4HoverShim = require('mq4-hover-shim');
    var autoprefixer = require('autoprefixer');

    grunt.initConfig({

        copy: {
            main: {
                src: 'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
                dest: 'app/scss/imports/_bootstrap-datepicker.scss'
            },
            swiper: {
                src: 'bower_components/Swiper/dist/css/swiper.css',
                dest: 'app/scss/imports/_swiper.scss'
            }
        },

        watch: {
            scripts: {
                files: ['app/js/**/*.js'],
                tasks: ['concat', 'uglify']
            },
            sass: {
                files: 'app/scss/**/*.scss',
                tasks: ['dist-css']
            },
        },

        sass: {
            dev: {
                options: {
                    includePaths: ['scss'],
                    precision: 6,
                    sourceComments: false,
                    sourceMap: true,
                    outputStyle: 'expanded'
                },
                files: {
                    'public/css/main.css': 'app/scss/main.scss'
                }
            }
        },

        postcss: {
            core: {
                options: {
                    map: true,
                    processors: [
                        mq4HoverShim.postprocessorFor({ hoverSelectorPrefix: '.bs-true-hover ' }),
                        autoprefixer
                    ]
                },
                src: 'public/css/*.css'
            }
        },

        cssmin: {
            options: {
                compatibility: 'ie9',
                keepSpecialComments: 0,
                sourceMap: true,
                advanced: false
            },
            dev: {
                files: [{
                    expand: true,
                    cwd: 'public/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'public/css',
                    ext: '.min.css'
                }]
            }
        },

        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'public/js/*.js',
                        'public/css/*.css',
                        'public/**/*.html'
                    ]
                },
                options: {
                    watchTask: true,
                    server: './public'
                }
            }
        },

        concat: {
            js: {
                options: {
                    separator: ';'
                },
                src: [
                    'bower_components/lodash/dist/lodash.min.js',
                    'bower_components/tether/dist/js/tether.min.js',
                    'bower_components/bootstrap/dist/js/bootstrap.min.js',
                    'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                    'bower_components/matchHeight/dist/jquery.matchHeight-min.js',
                    'bower_components/Swiper/dist/js/swiper.min.js',
                    'bower_components/bigSlide/dist/bigSlide.min.js',
                    'bower_components/magnific-popup/dist/jquery.magnific-popup.min.js',
                    'bower_components/select2/dist/js/select2.full.min.js',
                    'app/js/*.js'
                ],
                dest: 'public/js/main.js',
            }
        },

        uglify: {
            options: {
                compress: true,
                mangle: true,
                sourceMap: true
            },
            target: {
                src: 'public/js/main.js',
                dest: 'public/js/main.min.js'
            }
        }
    });

    // load npm tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-copy');


    // define default task
    grunt.registerTask('default', ['browserSync', 'copy', 'watch', 'concat', 'uglify', 'dist-css']);
    grunt.registerTask('dist-css', ['sass', 'postcss:core', 'cssmin']);
};
