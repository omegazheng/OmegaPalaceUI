# OmegaPalaceUI

## Overview

Compiled CSS, JS files and all HTML templates are found in /public and can be uses as-is. 

## Build system

To customise or add css or js you need to recompile with the included Gruntfile.
To use our build system you’ll need a copy of Node, Bower and Grunt.

Follow these steps to get started:

1. [Download and install Node](https://nodejs.org/download), which we use to manage our dev dependencies.
2. [Download and install Bower](https://bower.io/#install-bower), which we use to manage our front end dependencies.
3. Install the Grunt command line tools, grunt-cli, with npm install -g grunt-cli.
4. Navigate to the root /app directory and run;
  1. npm install
  2. bower install
  3. grunt